#!/usr/bin/env python
# coding: utf-8

# In[26]:


"""
SABESP system scraper

Author: Danilo Lessa Bernardineli (danilo.lessa@gmail.com)
"""


# In[27]:


## Dependences

import json
import requests as req
from bs4 import BeautifulSoup
import pandas as pd
from google.cloud import firestore
from google.cloud import storage
from datetime import datetime
import urllib3
from pathlib import Path
import base64
import time


# In[28]:


## Definitions

### 

urllib3.disable_warnings()


### Constants gcloud functions deploy delegator --runtime python37 --trigger-topic init_scrap


GCS_BUCKET = 'siges_sabesp'
FIRESTORE_COLLECTION = 'divisions'

USERNAME = 'vmnascimento.trail'
PASSWORD = 'Menasc@251099'

LOGIN_URI = 'https://siges.sabesp.com.br/Sigesweb/login.aspx'
MAPA_URI = 'https://siges.sabesp.com.br/Sigesweb/mapa_regiao.aspx?cUnidade=MO'


def get_session(login=USERNAME, password=PASSWORD) -> req.Session:
    """
    Generate an logged session.
    """ 
    ASPX_TRICK = {'__EVENTTARGET': '',
                  '__EVENTARGUMENT:': '',
                  '__VIEWSTATE': '',
                  "__LASTFOCUS": '',
                  '__VIEWSTATEGENERATOR': '',
                  '__VIEWSTATEGENERATOR': '',
                  '__SCROLLPOSITIONX': '0',
                  '__SCROLLPOSITIONY': '0'}
    s = req.Session()
    r = s.post(LOGIN_URI, 
           verify=False,
           data={"inputLogin": USERNAME,
                 "password": PASSWORD,
                 "loBt": "Confirmar",
                 **ASPX_TRICK})
    return s


def hidden_params(content: str, extra_params=None) -> dict:
    """
    Returns an dict payload with the hidden inputs in content (an html doc)
    and the extra_params key-values.
    """
    hidden_inputs = BeautifulSoup(content, 'lxml').find_all("input", type='hidden')
    params = {}
    for hidden_input in hidden_inputs:
        var, value = hidden_input['name'], hidden_input.get('value', '')
        params[var] = value
    params.update(extra_params)
    return params


def initialize_group(s: req.Session, group_id: str, code: str) -> tuple:
    """
    Returns the required viewstate and extra_params for starting
    scraping an group. Initial page content included.
    """
    extra_params = {"ctl00$ContentPlaceHolder1$dl_grupo": group_id,
                    "ctl00$ContentPlaceHolder1$dl_codigos": code}
    r = s.post(MAPA_URI, 
           verify=False,
           )
    viewstate = hidden_params(r.content, extra_params)
    params = {**viewstate}

    r = s.post(MAPA_URI,
               data=params,
               verify=False,
               )
    viewstate = hidden_params(r.content, extra_params)
    return r.content, viewstate, extra_params


def parse_group(content: str) -> dict:
    """
    Parses the page content for the initialized group content
    and returns informations about the divisions on it.
    Watch for tricks.
    """
    divisions_table = BeautifulSoup(content, 'lxml').findAll('table')[6]
    divisions_rows = divisions_table.findAll('tr', recursive=False)[1:]
    divisions = {}
    for division_row in divisions_rows:
        division_columns = division_row.findAll('td', recursive=False)
        division_param = division_columns[0].input['name']
        division_name = division_columns[1].span.text.strip()
        divisions[division_name] = division_param
    return divisions


def parse_summary(content:str, group_i) -> dict:
    """
    Parse everything in the summary page. Somewhat dirty and lots of tricks.
    Returns an three-layer nested dict.
    """
    soup = BeautifulSoup(content, 'lxml')
    
    division_selectors = ["""div > table > tr > td > table >
                             tr:nth-child({}) > td:nth-child(2)
                          """.format(i) for i in [group_i]]

    # Parse divisions
    divisions = [soup.select(selector)[0] for selector in division_selectors]
    divisions_list = []
    for division in divisions:
        # Division values
        division_name = division.span.text.strip()
        division_id = division.find('input', type='hidden')['value']

        # Parse teams
        divisions_rows = division.table.findAll('tr', recursive=False)
        teams = divisions_rows[1::2]
        team_activities = divisions_rows[2::2]        
        teams_list = []
        for i, team in enumerate(teams):
            # Team values
            team_name = team.findAll('span')[1].text.strip()
            team_id = team.findAll('span')[0].text.strip()

            # Parse activities
            activities = []
            activity_rows = team_activities[i].findAll('tr')
            for activity_row in activity_rows:
                # Activity values
                activity_cols = activity_row.findAll('td')
                activity_link = (activity_cols[2].span['onclick']
                                                 .split("location=")[1]
                                                 .split(";")[0]
                                                 .replace("\\", "")
                                                 .replace("'", ""))
                activity_id = activity_cols[2].text.strip()
                activity_name = activity_cols[3].text.strip()
                activity_address = activity_cols[4].text.strip()

                # Append everything
                activity_element = {'id': activity_id,
                                    'name': activity_name,
                                    'address': activity_address,
                                    'uri': activity_link}
                activities.append(activity_element)
            team_element = {'id': team_id,
                            'name': team_name,
                            'activities': activities}
            teams_list.append(team_element)
        division_element = {'id': division_id,
                            'name': division_name,
                            'teams': teams_list}
        divisions_list.append(division_element)
        return divisions_list
    
    
def download_activity(s: req.Session, uri: str) -> str:
    """
    
    """
    return s.post(uri, verify=False).content


def get_selector(soup, selector: str) -> str:
    """
    Helper function for getting field content.
    """
    return soup.select(selector)[0].text.strip()


def parse_activity_page(content: str) -> dict:
    """
    Dirty function for parsing the activity page. Be trick beware.
    """
    soup = BeautifulSoup(content, 'lxml')
    output = {
        "servico": get_selector(soup, '#ctl00_ContentPlaceHolder1_tx_descricao'),
        'rgi': get_selector(soup, '#ctl00_ContentPlaceHolder1_lb_nr_rgi'),
        "num_solic": get_selector(soup, '#ctl00_ContentPlaceHolder1_lb_num_solicitacao'),
        'codif': get_selector(soup, '#ctl00_ContentPlaceHolder1_lb_codif_sabesp'),
        'doc_origem': get_selector(soup, '#ctl00_ContentPlaceHolder1_lb_doc_origem'),
        'endereco': get_selector(soup, '#ctl00_ContentPlaceHolder1_lb_endereco'),
        'signosnet': get_selector(soup, '#ctl00_ContentPlaceHolder1_lnkSignosNet'),
        'data_solic': get_selector(soup, '#ctl00_ContentPlaceHolder1_lb_dt_acatamento'),
        'guia_referencia': get_selector(soup, '#ctl00_ContentPlaceHolder1_lb_referencia_guia'),
        'bairro': get_selector(soup, '#ctl00_ContentPlaceHolder1_lb_bairro'),
        'municipio': get_selector(soup, '#ctl00_ContentPlaceHolder1_lb_cidade'),
        'cod_log': get_selector(soup, '#ctl00_ContentPlaceHolder1_lb_cod_logradouro'),
        'cod_area': get_selector(soup, '#ctl00_ContentPlaceHolder1_lb_cod_area'),
        'reference_point': get_selector(soup, '#ctl00_ContentPlaceHolder1_lb_ponto_referencia'),
        'cliente': get_selector(soup, '#ctl00_ContentPlaceHolder1_lb_cliente'),
        'telefone': get_selector(soup, '#ctl00_ContentPlaceHolder1_lb_telefone'),
        'pagto': get_selector(soup, 'div > div:nth-child(2) > div > table > tr:nth-child(14) > td:nth-child(2) > span'),
        'sit_ligacao': get_selector(soup, 'div > div:nth-child(2) > div > table > tr:nth-child(20) > td'),
        'info_compl': get_selector(soup, '#ctl00_ContentPlaceHolder1_lb_complemento'),
        'uma': get_selector(soup, 'div > div:nth-child(2) > div > table > tr:nth-child(24) > td:nth-child(1)'),
        'tipo_cavalete': get_selector(soup, 'div > div:nth-child(2) > div > table > tr:nth-child(24) > td:nth-child(2)'),
        'qtd_cavalete': get_selector(soup, 'div > div:nth-child(2) > div > table > tr:nth-child(24) > td:nth-child(3)'),
        'hidrometro_cad': get_selector(soup, 'div > div:nth-child(2) > div > table > tr:nth-child(27) > td:nth-child(1)'),
        'diametro_ramal': get_selector(soup, 'div > div:nth-child(2) > div > table > tr:nth-child(27) > td:nth-child(2)'),
        'cap_hidro': get_selector(soup, 'div > div:nth-child(2) > div > table > tr:nth-child(27) > td:nth-child(3)'),
        'etapas': pd.read_html(str(soup.select('#ctl00_ContentPlaceHolder1_grd_etapas')[0]), header=0)[0] 
    }
    return output


def process_activities(s: req.Session, division_list: list,
                       upload_base_path: Path, bucket,
                       default_str) -> None:
    """
    
    Uses side-effects.
    """
    upload_template = str(upload_base_path / '{division}/{team}/{activity}.html')
    upload_params = {}
    
    base_uri = 'https://siges.sabesp.com.br/Sigesweb/'
    for division in division_list:
        team_list = division['teams']
        upload_params['division'] = division['id']
        for team in team_list:
            activity_list = team['activities']
            upload_params['team'] = team['id']
            for activity in activity_list:
                uri = base_uri + activity['uri']
                content = download_activity(s, uri)
                upload_params['activity'] = activity['id']
                upload_path = upload_template.format(**upload_params)
                print(default_str + "activity uploaded at " + upload_path)
                blob = bucket.blob(upload_path)
                blob.upload_from_string(str(content))                
                out = parse_activity_page(content)
                activity.update(out)
    return None



def serialize_divisions(divisions_list: list) -> None:
    """
    Transforms the division list into something serializable.
    Uses side-effects.
    """
    dict_out = divisions_list.copy()
    for division in dict_out:
        team_list = division['teams']
        for team in team_list:
            activity_list = team['activities']
            for activity in activity_list:
                activity['etapas'] = activity['etapas'].to_dict(orient='records')
    return None


def upload_data_to_firestore(divisions_list: list) -> None:
    db = firestore.Client()
    for division in divisions_list:
        doc_ref = db.collection(FIRESTORE_COLLECTION).document(division['id'])
        doc_ref.set(division)
        team_list = division['teams']
        for team in team_list:
            team_ref = doc_ref.collection('teams').document(team['id'])
            team_ref.set(team)
            activity_list = team['activities']
            for activity in activity_list:
                activity_ref = team_ref.collection('activities').document(activity['id'])
                activity_ref.set(activity)
    return None


def group_scrap(group_id: str, division_i: int, code: str, timestamp: datetime) -> list:
    """
    Scrap and process all the data with an given group and code type.
    """
    default_str = "{}|{}|{}|{} ".format(group_id, division_i, code, timestamp)
    print(default_str + "started")
    t0 = time.time()
    execution_timestamp = timestamp
    upload_base_path = Path("{}/{}/{}".format(execution_timestamp, code, group_id))
    
    # Initialize
    s = get_session()
    group_content, viewstate, extra_params = initialize_group(s, group_id, code)
    group_divisions = parse_group(group_content)

    
    # Expand the divisions
    if division_i >= len(group_divisions.items()):
        print(default_str + "aborted due to length mismatch.".format(group_id, division_i, code, timestamp))
        return (False, None)
    
    group_division = list(group_divisions.items())[division_i]
    (division_name, division_param) = group_division
    params = {**viewstate,
              division_param: '+'}
    r = s.post(MAPA_URI,
               verify=False,
               data=params)
    viewstate = hidden_params(r.content, extra_params)
    t = time.time()
    print(default_str + "division expanded - {:.1f}s".format(t - t0))

    
    # Parse the teams on the divisions    
    soup = BeautifulSoup(r.content, 'lxml')
    group_i = [i + 2 for i in range(len(group_divisions))]
    group_i = [group_i[division_i]] # division_i
    division_selectors = ['tr:nth-child({}) > td:nth-child(2) > table'.format(i) for i in group_i]
    division_tables = [soup.select(selector)[0] for selector in division_selectors]
    team_params = []
    for i, division_table in enumerate(division_tables):
        team_rows = division_table.findAll('tr')[1:]
        for team_row in team_rows:
            team_cols = team_row.findAll('td')
            team_param = team_cols[0].input['name']
            team_name = team_cols[1].text.strip()
            team_params.append(team_param)

            
    # Expand the teams
    for i, team_param in enumerate(team_params):
        params = {**viewstate,
                  team_param: '+'}
        r = s.post(MAPA_URI,
                   verify=False,
                   data=params)
        viewstate = hidden_params(r.content, extra_params)   
        t = time.time()
        print(default_str + "team expanded {}/{} - {:.1f}s".format(i + 1, len(team_params), t - t0))
        
    
    # Get and parse all activities
    group_content = r.content
    division_list = parse_summary(group_content, division_i + 2)
    gcs_client = storage.Client()
    bucket = gcs_client.bucket(GCS_BUCKET)
    summary_path = str(upload_base_path / division_list[0]['id'] / 'summary.html')
    blob = bucket.blob(summary_path)
    blob.upload_from_string(str(group_content))
    t = time.time()
    print(default_str + "summary uploaded at {}  - {:.1f}s".format(summary_path, t - t0))
    
    process_activities(s, division_list, upload_base_path, bucket, default_str)
    serialize_divisions(division_list)
    upload_data_to_firestore(division_list)
    t = time.time()
    print(default_str + "scraped  - {:.1f}".format(t - t0))
    return True, division_list


def pubsub_handler(event, context):
    pubsub_message = base64.b64decode(event['data']).decode('utf-8')
    pubsub_object = json.loads(pubsub_message)
    group_id = pubsub_object['group_id']
    code = pubsub_object['code']
    division_i = int(pubsub_object['division_i'])
    timestamp = pubsub_object['timestamp']
    return group_scrap(group_id, division_i, code, timestamp)[0]

if __name__ == "__main__":
    bytecode = json.dumps({"group_id": "99", "code": "0", "division_i": 2, "timestamp": datetime.utcnow()}, default=str).encode("utf-8")
    content = base64.b64encode(bytecode)
    test_data = {"data": content}
    print(pubsub_handler(test_data, None))
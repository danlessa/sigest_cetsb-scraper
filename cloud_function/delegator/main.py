import yaml
import json
from datetime import datetime
from google.cloud import pubsub_v1

PROJECT_ID = "master-mote-259518"
TOPIC_NAME = "scrap_group"

def pubsub_obj(group_id, i, timestamp):
    pubsub_obj = {'group_id': group_id,
                  'code': '0',
                  'division_i': i,
                  'timestamp': timestamp}
    return pubsub_obj


def delegate_groups():    
    with open("params.yaml", 'r') as fid:
        params = yaml.safe_load(fid.read())
        
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(PROJECT_ID, TOPIC_NAME)
    timestamp = datetime.utcnow()
    for group_id, group_name in params['groups'].items():
        for i in range(0, 4):
            padded_group_id = "{}".format(group_id).rjust(2, "0")
            message = pubsub_obj(padded_group_id, i, timestamp)
            data = json.dumps(message, default=str).encode('utf-8')
            publisher.publish(topic_path, data=data)  

        
def pubsub_handler(event, context):
    return delegate_groups()

if __name__ == "__main__":
    delegate_groups()
# SIGES-SABESP Scraper

*Author: Danilo Lessa Bernardineli (danilo.lessa@gmail.com*

Esse repositório contém uma POC de um scraper que coleta informações do SIGES da SABESP e os armazena tanto em um bucket do Google Cloud Storage quanto em uma coleção do Google Firestore.

## Infra

### Components

* Pubsub topics (scrap_group and init_scrap)
* Firestore (divisions collection)
* GCS (siges_sabesp bucket)
* CCF (scraper and delegator)
* Cloud Scheduler ()

### Outputs

* hourly execution between 7-20 everyday at UTC-3
* divisions collection at Firestore
* summary and activity files at siges_sabesp bucket
    * summary path: {timestamp}/{code}/{group_id}/{division_id}/summary.html
    * activities path: {timestamp}/{code}/{group_id}/{division_id}/{team_id}/{activity_id}.html

### Params

* Tamanho esperado para cada combinação: ~1MB
* Número de documentos para cada combinação: ~40
* Tempo de execução: ~5min


## Como executar

Execute a função 'group_scrap' no arquivo main.py fornecendo um número identificado o grupo, e outro identificado o código. Adapte conforme a necessidade.

Mude as constantes definidas no inicio do arquivo para se adequar ao contexto (login, senha, bucket do GCS e coleção do firestore)

Não esqueça de criar o bucket, ativar o firestore e fazer o upload da função no console do google.

Caso você queira fazer rodar de modo periodizado, use um cronjob que execute a função especificada junto a uma uma lista de combinações de grupos / tipos de códigos. No Google, isso é implementado via o Cloud Scheduler.


## Avisos

Esse código é uma POC. O seu funcionamento foi validado de modo preliminar, mas a escrita está bem longe de estar refinada e adequada para com as boas práticas.


## Como funciona

O SIGES é uma plataforma que se utiliza amplamente de um parâmetro de estado (VIEWSTATE) para controlar o fluxo do que se é visualizado. 

Esse scraper faz a extração das informações relevantes através da simulação da interação de usuário conforme o gerenciamento do VIEWSTATE esperado para se obter as informações de todas as atividades que estão acontecendo em um dado momento.

Uma vez que se há 